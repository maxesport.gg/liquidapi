# -*- coding: utf-8 -*-
# © Maxesport/Théo Matis
import requests
import json
import time
from fractions import Fraction

headers = {'User-Agent': "Maxesport.gg (theo.matis@maxesport.gg)"}
#playerList= ["ZywOo"]
playerList = ["ZywOo","RpK","ApEX","Shox","KennyS","JaCkz","AmaNEk","Nexa","HuNter-","ALEX_(British_player)"]
monthList = ["Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Août","Septembre","octobre","Novembre","Décembre"]
teamLink = {"Team Vitality":"https://maxesport.gg/marque/vitality" ,"G2 Esports":"https://maxesport.gg/marque/g2-esports"}
toGet = ["mouse+brand","mouse+model","dpi","sensitivity","acceleration","yaw","raw+input","keyboard+brand","keyboard+model","mousepad+brand","mousepad+model","headset+brand","headset+model","headset+model","monitor+brand","monitor+model","refresh+rate","scaling","dpi","polling+rate","team","name","nationality","role","twitter","facebook","instagram","twitch+stream","resolution+width","resolution+height","windows+sensitivity","zoom+sensitivity","inverted+mouse","age","birth_day","crosshair+size","crosshair+style","crosshair+dot","crosshair+gap","crosshair+alpha","crosshair+thickness","crosshair+thickness","crosshair+outline+thickness","crosshair+color"]
separatorPrintouts = "%7C"
def getProductJson():
    url = "https://maxesport.gg/export-product-as-json"
    S = requests.Session()
    R = S.get(url=url,headers=headers)
    return R.json()
productJson = getProductJson()
def getOuts():
    ret = ""
    for a in toGet:
        if ret=="":
            ret=ret+"Has+"+a
        else:
            ret=ret+separatorPrintouts+"Has+"+a
    return ret
def getName(n):
    try:
        return oldJson["query"]["results"][0][playerName]["printouts"][n][0]
    except:
        return "-"
def getNameFulltext(n):
    try:
        return oldJson["query"]["results"][0][playerName]["printouts"][n][0]["fulltext"]
    except:
        return "-"
def getMaxesportBrandLink(brand):
    return ("https://maxesport.gg/marque/"+brand.replace(" ", "-")).lower()
def getMaxesportModelLink(brand,model):
    brandn = brand.replace(" ", "-")
    modeln = model.replace(" ", "-")
    return ("https://maxesport.gg/produit/"+brandn+"-"+modeln).lower()
def getComp(maxesportProduct,liquidProduct):
    translationList = {"noir":"black","rouge":"red","carbone":"carbon","bleu":"blue","blanc":"white","mat":"matte","brillant":"glossy","jaune":"yellow","sans fil":"wireless"}
    maxesportProduct = maxesportProduct.lower()
    liquidProduct = liquidProduct.lower()
    liquidProduct = liquidProduct.replace("(","")
    liquidProduct = liquidProduct.replace(")","")

    maxesportProduct = maxesportProduct.replace(" - ", " ")
    for translation in translationList:
        maxesportProduct = maxesportProduct.replace(translation,translationList[translation])
    score = 0
    maxesportProductTable = maxesportProduct.split()
    liquidProductTable = liquidProduct.split()
    for keyWord in liquidProductTable:
        if keyWord in maxesportProductTable:
            score=score+20+len(keyWord)
    return score
def getMaxesportId(liquidProduct,productType):
    if productType=="monitor":
        return -1
    idJson = productJson

    scoreTable = {}
    max = 0
    selectedName = "-"
    selectedId = -1
    for maxesportProduct in idJson[productType]:
        scoreTable[maxesportProduct] = getComp(maxesportProduct, liquidProduct)
    for product in scoreTable:
        if scoreTable[product]>max:
            max = scoreTable[product]
            selectedName = product
            selectedId = idJson[productType][product]
        elif scoreTable[product]==max:
            if len(product)<len(selectedName):
                selectedName = product
                selectedId = idJson[productType][product]
    return selectedId
def multiplySafe(nbr1,nbr2):
    try:
        return nbr1*nbr2
    except:
        return "-"
def getRawBirthDate():
    try:
        return oldJson["query"]["results"][0][playerName]["printouts"]["Has birth_day"][0]["raw"]
    except:
        return "-"
def formatpseudo(n):
    if n == "ALEX (British player)":
        return "ALEX"
    else:
        return n
def getBirthDate():
    rawBirthDate = getRawBirthDate()
    rawTable = []
    if rawBirthDate=="-":
        return "-"
    else:
        rawTable = rawBirthDate.split("/")
        year = rawTable[1]
        month = monthList[int(rawTable[2])-1]
        day = rawTable[3]
        return str(day) + " " + str(month) + " " + str(year) + " "
def getInstagramCar(link):
    instagram = link.replace("https://www.instagram.com/","")
    return '[woodmart_instagram data_source="ajax" number="4" username="'+instagram+'" per_row="4" spacing="0" rounded="0" hide_mask="0"][/woodmart_instagram]'
def getRatio(w,h):
    try:
        return str(Fraction(w,h))
    except:
         return "-"
def downloadCfg(name):
    exp = {"ALEX (British player)":"alex","hunter-":"hunter"}
    if name in exp:
        name = exp[name]
    name = name.lower()
    url = 'https://prosettings.net/configs/'+name+'.zip'
    r = requests.get(url,headers=headers)
    for e in exp:
        if(exp[e]==name):
            name = e
    with open('medias/configs/'+name+'.zip', 'wb') as f:
        f.write(r.content)
    return '<a href="https://maxesport.gg/medias/configs/'+name+'.zip"><button class="btn btn-color-primary btn-style-default btn-shape-semi-round btn-size-small">Télécharger</button></a>'
def getTeamLink(link):
    return '<a href="'+str(teamLink[link])+'">'
def genCrosshair(alpha,color,dot,gap,size,style,usealpha,thickness,sniper_width,drawoutline):
    return "cl_crosshairalpha "+str(alpha)+"; cl_crosshaircolor "+str(color)+"; cl_crosshairdot "+str(dot)+"; cl_crosshairgap "+str(gap)+"; cl_crosshairsize "+str(size)+"; cl_crosshairstyle "+str(style)+"; cl_crosshairusealpha "+str(usealpha)+"; cl_crosshairthickness "+str(thickness)+"; cl_crosshair_sniper_width "+str(sniper_width)+"; cl_crosshair_drawoutline "+str(drawoutline)+";"
for playerName in playerList:
    URL = "https://liquipedia.net/counterstrike/api.php?action=askargs&api_version=3&conditions="+playerName+"&printouts="+getOuts()+"&format=json"
    S = requests.Session()
    print("[INFO] Fetching: "+playerName+".. ")
    R = S.get(url=URL,headers=headers)
    playerName = playerName.replace("_"," ")
    print("[INFO] Fetching done")
    oldJson=R.json()
    print("[INFO] Dowloading config..")
    downloadCfg(playerName)
    print("[INFO] Dowloaded")

    newStrut = {
        "pseudo": {"data": formatpseudo(playerName)},
        "prenom": {"data": getName("Has name").split()[0]},
        "nom": {"data": getName("Has name").split()[1]},
        "souris_marque": {"data": getName("Has mouse brand"),"potential_link":getMaxesportBrandLink(getName("Has mouse brand"))},
        "souris_model": {"data": getName("Has mouse model"),"potential_id":getMaxesportId(getName("Has mouse model"),"mouse")},
        "souris_sensitivity": {"data": getName("Has sensitivity")},
        "souris_polling": {"data": getName("Has polling rate")},
        "souris_dpi": {"data": getName("Has dpi")},
        "souris_edpi": {"data": multiplySafe(getName("Has dpi"),getName("Has sensitivity"))},
        "souris_acceleration": {"data": getNameFulltext("Has acceleration")},
        "souris_yaw": {"data": getNameFulltext("Has yaw")},
        "souris_raw_input": {"data": getNameFulltext("Has raw input")},
        "clavier_marque": {"data": getName("Has keyboard brand"),"potential_link":getMaxesportBrandLink(getName("Has keyboard brand"))},
        "clavier_model": {"data": getName("Has keyboard model"),"potential_id":getMaxesportId(getName("Has keyboard model"),"keyboard")},
        "tapis_marque": {"data": getName("Has mousepad brand"),"potential_link":getMaxesportBrandLink(getName("Has mousepad brand"))},
        "tapis_model": {"data": getName("Has mousepad model"),"potential_id":getMaxesportId(getName("Has mousepad model"),"mousepad")},
        "casque_marque": {"data": getName("Has headset brand"),"potential_link":getMaxesportBrandLink(getName("Has headset brand"))},
        "casque_model": {"data": getName("Has headset model"),"potential_id":getMaxesportId(getName("Has headset model"),"headset")},
        "ecran_marque": {"data": getName("Has monitor brand"),"potential_link":getMaxesportBrandLink(getName("Has monitor brand"))},
        "ecran_model": {"data": getName("Has monitor model"),"potential_id":getMaxesportId(getName("Has monitor model"),"monitor")},
        "ecran_taux": {"data": getName("Has refresh rate")},
        "ecran_resolution": {"data": str(getName("Has resolution width"))+"x"+str(getName("Has resolution height"))},
        "ecran_scalling": {"data": getName("Has scaling")},
        "ecran_ratio":{"data":getRatio(getName("Has resolution width"),getName("Has resolution height"))},
        "equipe": {"data": getNameFulltext("Has team")},
        "country": {"data": getName("Has nationality")},
        "role": {"data": getName("Has role")},
        "twitter": {"data": getName("Has twitter")},
        "facebook": {"data": getName("Has facebook")},
        "instagram": {"data": getName("Has instagram")},
        "twitch": {"data": getName("Has twitch stream")},
        "windows_sensitivity": {"data": getName("Has windows sensitivity")},
        "zoom_sensitivity": {"data": getName("Has zoom sensitivity")},
        "souris_inverted": {"data": getNameFulltext("Has inverted mouse")},
        "jeu": {"data": "CS:GO"},
        "birth_date":{"data": getBirthDate()+ "("+str(getName("Has age"))+" ans)"},
        "code_crosshair":{"data":genCrosshair(getName("Has crosshair alpha"),getName("Has crosshair color"),getName("Has crosshair dot"),getName("Has crosshair gap"),getName("Has crosshair size"),getName("Has crosshair style"),1,getName("Has crosshair thickness"),1,0)},
        "instagram_carrousel":{"data":getInstagramCar(getName("Has instagram"))},
        "equipe_link":{"data":str(getTeamLink(str(getNameFulltext("Has team"))))},
        "cfg_button": {"data": downloadCfg(playerName)}
    }
    print("[INFO] Writing on file ...")
    f = open("json/"+playerName+".json","w+")
    f.write(json.dumps(newStrut))
    f.close()

    print("[INFO] Writing done")
    print("[INFO] Waiting 30s")
    time.sleep(30)
print("[INFO] FINISHED")
